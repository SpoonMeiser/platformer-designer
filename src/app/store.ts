import { configureStore } from "@reduxjs/toolkit"
import paletteReducer from "../features/palette/paletteSlice"
import gridReducer from "../features/tilemap/gridSlice"

export const store = configureStore({
    reducer: {
        palette: paletteReducer,
        grid: gridReducer,
    },
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
