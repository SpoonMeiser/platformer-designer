import styled from 'styled-components'

const Code = styled.div`
    white-space: pre;
    font-family: monospace;
`

export default Code
