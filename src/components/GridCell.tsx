import styled, { css } from 'styled-components'

import TileDiv from "./TileDiv"

const GridCell = styled(TileDiv).attrs(props => ({
    style: {
        gridColumn: props.x + 1,
        gridRow: props.y + 1,
    }
}))`
    border: 1px solid #ccf;
`

export default GridCell
