import styled from 'styled-components'

import buttonBase from "./buttonBase"

const Button = styled.button`
    ${buttonBase}
`

export default Button
