import styled from 'styled-components'

const Grid = styled.div`
    display: grid;
    gap: 0;
    justify-content: center;
`

export default Grid
