import styled from 'styled-components'

const Fieldset = styled.fieldset`
    display: inline-block;
    border-radius: 1ex;
    border: 0.2ex solid #ccd;
`

export default Fieldset
