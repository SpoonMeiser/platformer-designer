import { css } from 'styled-components'

const depressedButton = css`
    box-shadow: 0 0.1ex 0 #88a, 0.1ex 0.1ex 0.1ex rgba(0, 0, 0, 0.3);
    top: -0.1ex;
`

const buttonBase = css`
    padding: 0.5ex 0.6em;
    border-radius: 1ex;
    background-color: #eef;
    border: 0.2ex solid #ccd;
    margin: 0.1ex 0.2em;
    outline: 0;
    position: relative;
    top: -0.6ex;
    box-shadow: 0 0.6ex 0 #88a, 0.6ex 0.6ex 2ex rgba(0, 0, 0, 0.3);
    transition: box-shadow 0.1s, top 0.1s;

    :active {
        ${depressedButton}
    }

    ${props => props.selected && depressedButton}
`

export default buttonBase
