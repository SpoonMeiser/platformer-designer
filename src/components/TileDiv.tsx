import styled, { css } from 'styled-components'

const getDivColour = (tile: string): string => {
    switch (tile) {
        case 'X':
            return 'black'
        case ' ':
            return 'white'
        case '-':
            return 'blue'
    }
}


const TileDiv = styled.div`
    width: ${props => props.size};
    height: ${props => props.size};
    background-color: ${props => getDivColour(props.tile)}
`

TileDiv.defaultProps = {
    size: '1em'
}

export default TileDiv
