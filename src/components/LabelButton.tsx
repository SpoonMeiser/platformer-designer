import styled from 'styled-components'

import buttonBase from "./buttonBase"

const LabelButton = styled.label`
    ${buttonBase}

    > input[type=radio] {
        display: none;
    }
`

export default LabelButton
