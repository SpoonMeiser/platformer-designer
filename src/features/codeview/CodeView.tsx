import * as React from "react"

import { useAppSelector, useAppDispatch } from "../../app/hooks"
import Code from "../../components/Code"

const CodeView = props => {
    const tilemapData = useAppSelector(state => state.grid.data)
    const screenName = "screen"
    const screenTitle = "Descriptive Title"

    const screenData = {
        [screenName]: {
            "title": screenTitle,
            "tilemap": tilemapData,
        }
    }

    return <Code>
        { JSON.stringify(screenData, null, 4) }
    </Code>
}

export default CodeView
