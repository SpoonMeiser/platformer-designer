import { createSlice, PayloadAction } from "@reduxjs/toolkit"

import TileType from "../../types/TileType"

interface PaletteState {
    tile: TileType
}

const initialState: PaletteState = {
    tile: "X"
}

const paletteSlice = createSlice({
    name: "palette",
    initialState,
    reducers: {
        setTile: (state, action: PayloadAction<TileType>) => {
            state.tile = action.payload
        }
    }
})

export const { setTile } = paletteSlice.actions

export default paletteSlice.reducer
