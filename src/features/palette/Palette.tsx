import * as React from "react"

import { useAppSelector, useAppDispatch } from "../../app/hooks"
import { setTile } from "./paletteSlice"
import TileDiv from "../../components/TileDiv"
import LabelButton from "../../components/LabelButton"
import Button from "../../components/Button"
import Fieldset from "../../components/Fieldset"
import { RootState } from "../../app/store"

import { clear } from "../tilemap/gridSlice"

const handleTileSelect = dispatch => e => dispatch(setTile(e.target.value))

const PaletteTileRadio = ({ tile }) => {
    const selectedTile = useAppSelector(state => state.palette.tile)
    const dispatch = useAppDispatch()

    const selected = selectedTile == tile

    return <LabelButton selected={selected}>
        <input
            type="radio"
            name="tile" value={tile}
            checked={selected}
            onChange={handleTileSelect(dispatch)}
        />
        <TileDiv tile={tile} style={{display: "inline-block"}} />
    </LabelButton>
}

const Palette = (props) => {
    const dispatch = useAppDispatch()

    return <div>
        <Fieldset>
            <legend>Tile</legend>
            <PaletteTileRadio tile="X" />
            <PaletteTileRadio tile="-" />
            <PaletteTileRadio tile=" " />
        </Fieldset>
        <Button onClick={() => dispatch(clear({}))}>
            Clear
        </Button>
    </div>
}

export default Palette
