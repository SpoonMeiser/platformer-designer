import * as React from "react"
import { useStore } from "react-redux"

import { useAppSelector, useAppDispatch } from "../../app/hooks"
import GridCell from "../../components/GridCell"
import Grid from "../../components/Grid"
import { addTileAt } from "./gridSlice"

const Tile = ({x, y}) => {
    const currentTile = useAppSelector(state => state.grid.data[y][x])
    const dispatch = useAppDispatch()

    const store = useStore()
    const getSelectedTile = () => store.getState().palette.tile

    const mousePaintHandler = e => {
        const primaryButton = 1
        if (e.buttons & primaryButton) {
            dispatch(addTileAt({x, y, tile: getSelectedTile()}))
        }
    }

    return <GridCell
        x={x}
        y={y}
        tile={currentTile}
        onMouseEnter={mousePaintHandler}
        onMouseDown={mousePaintHandler}
    />
}

const Tilemap = () => {

    let gridcells: JSX.Element[] = new Array()
    for (let x = 0; x < 50; x++)
        for (let y = 0; y < 25; y++) {
            gridcells.push(<Tile key={`${x}:${y}`} x={x} y={y} />)
        }

    // onMouseDown handler to prevent drag-n-drop behaviour
    return <Grid onMouseDown={e => e.preventDefault()}>
        { gridcells }
    </Grid>
}

export default Tilemap
