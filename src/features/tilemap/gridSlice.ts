import { createSlice, PayloadAction } from "@reduxjs/toolkit"

import TileType from "../../types/TileType"

const getEmptyTilemapData = () => new Array(25).fill(" ".repeat(50))

interface AddTilePayload {
    x: number,
    y: number,
    tile: TileType,
}

interface GridState {
    data: string[]
}

const initialState: GridState = {
    data: getEmptyTilemapData(),
}

const gridSlice = createSlice({
    name: "grid",
    initialState,
    reducers: {
        addTileAt: (state, action: PayloadAction<AddTilePayload>) => {
            const {x, y, tile} = action.payload
            const orig_row = state.data[y]
            const new_row = orig_row.substr(0, x) + tile + orig_row.substr(x + 1)

            state.data[y] = new_row
        },
        clear: (state, action) => {
            state.data = getEmptyTilemapData()
        },
    }
})

export const { addTileAt, clear } = gridSlice.actions

export default gridSlice.reducer
