import * as React from "react"
import Palette from "./features/palette/Palette"
import Tilemap from "./features/tilemap/Tilemap"
import CodeView from "./features/codeview/CodeView"

const App = (props) => <>
    <Palette />
    <Tilemap />
    <CodeView />
</>

export default App
