import * as React from "react"
import ReactDOM from "react-dom"
import { Provider, useDispatch, useSelector } from "react-redux"

import { store } from "./app/store"
import App from "./App"

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById("app")
)
